function initApp() {
  const txtEmail = document.getElementById('inputEmail');
  const txtPassword = document.getElementById('inputPassword');
  const btnLogin = document.getElementById('btnLogin');
  const btnGoogle = document.getElementById('btnGoogle');
  const btnSignUp = document.getElementById('btnSignUp');

  function loginSuccess() {
    create_alert("success", "login succeeded");
    window.location = "post-page.html";
  }

  function loginFail(error) {
    create_alert("error", error.message);
    txtEmail.value = "";
    txtPassword.value = "";
  }

  btnLogin.addEventListener('click', function () {
    firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value)
      .then(loginSuccess).catch(loginFail);
  });

  btnGoogle.addEventListener('click', function () {
    let provider = new firebase.auth.GoogleAuthProvider();

    firebase.auth().signInWithPopup(provider)
      .then(loginSuccess).catch(loginFail);
  });

  btnSignUp.addEventListener('click', function () {        
    firebase.auth().createUserWithEmailAndPassword(txtEmail.value, txtPassword.value)
      .then(loginSuccess).catch(loginFail);
  });
}

function create_alert(type, message) {
  const alertarea = document.getElementById('custom-alert');
  if (type == 'success') {
    str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
    alertarea.innerHTML = str_html;
  }
  else if (type == 'error') {
    str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
    alertarea.innerHTML = str_html;
  }
}

window.onload = function () {
  initApp();
};
