function changeUserName(user) {
	const userName = document.getElementById('user-dropdown');
	userName.innerHTML = user.email;
}

function submitPost(user) {
    const postBtn = document.getElementById('post_btn');
    const postTxt = document.getElementById('comment');
    const postTitle = document.getElementById('title');

    postBtn.addEventListener('click', function () {
        if (postTxt.value != '') {
            firebase.database().ref('post_list').push({
                email: user.email,
                title: postTitle.value,
                post: postTxt.value
            }).then(function() {
                postTxt.value = '';
                postTitle.value = '';
                window.location.href = 'post-list.html';
            });
        }
    });

}

function initApp() {
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
			changeUserName(user);
			submitPost(user);
        }
        else {
            console.log('No user logged in.');
        }
    });

}

window.onload = function() {
    initApp();
};
