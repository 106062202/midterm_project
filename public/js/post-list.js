function changeUserName(user) {
	const userName = document.getElementById('user-dropdown');
	userName.innerHTML = user.email;
}

function expandPost(id) {
    const post = document.getElementById(id);
    const postRef = firebase.database().ref('post_list/' + id);

    postRef.once('value')
        .then(function(snapshot) {
            const postTxt = post.querySelector('.card-text');
            postTxt.innerHTML = snapshot.val().post;
        })
        .catch(function(e) {
            console.log(e.message);
        });

    post.querySelector('.btn-primary').remove();

    post.innerHTML += '<div class="my-3 p-3 bg-white rounded shadow-sm"><h6 class="border-bottom border-gray pb-2 mb-0">Comments</h6></div>';

    const commentsRef = firebase.database().ref('post_list/' + id + '/comments');
    const totalComments = [];

    commentsRef.once('value')
        .then(function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
                totalComments.push('<div class="media text-muted pt-3"><p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray"><strong class="d-block text-gray-dark">@' + childSnapshot.val().user + '</strong>' + childSnapshot.val().comment + '</p></div>');
                post.querySelector('.rounded').innerHTML = totalComments.join('');
            });
        })
        .catch(function(e) {
            console.log(e.message);
        });

    post.innerHTML += "<button class='btn btn-success' onclick='expandComment(\"" + id + "\")'>Comment</button>";
}

function expandComment(id) {
    const post = document.getElementById(id);
    post.querySelector('.btn-success').remove();

    post.innerHTML += "<div class='p-3 bg-white rounded box-shadow'><h5 class='border-bottom border-gray pb-2 mb-0'>New Comment</h5><textarea class='form-control' rows='5' id='comment' style='resize:none'></textarea><div class='media text-muted pt-3'><button id='post_btn' type='button' class='btn btn-success' onclick='submitComment(\"" + id + "\")'>Submit</button></div></div>";
}

function submitComment(id) {
    const commentsRef = firebase.database().ref('post_list/' + id + "/comments");
    const submitTxt = document.getElementById(id).querySelector('.form-control');
    const totalComments = [];

    commentsRef.push({
        user: firebase.auth().currentUser.email,
        comment: submitTxt.value
    })

    submitTxt.value = '';

    commentsRef.on('child_added', function(snapshot) {
        const np = snapshot.val();

        totalComments.push('<div class="media text-muted pt-3"><p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray"><strong class="d-block text-gray-dark">@' + np.user + '</strong>' + np.comment + '</p></div>');

        document.getElementById(id).querySelector('.rounded').innerHTML = totalComments.join('');
    });

}

function loadPost() {
    const listRef = firebase.database().ref('post_list');
    const posts = [];

    listRef.once('value')
        .then(function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
                const child = childSnapshot.val();
                const key = childSnapshot.key;

                const str1 = "<div class='card mb-4' id='"; // + child.key
                const str2 = "'><div class='card-body'><h2 class='card-title'>"; // + child.title
                const str3 = "</h2><p class='card-text'></p><button class='btn btn-primary' onclick='expandPost(\"";
                const str4 = "\")'>Read More</button>";
                const str5 = "</div><div class='card-footer text-muted'>Posted by "; // + child.email
                const str6 = '</div></div>\n';

                const e = str1+key+str2+child.title+str3+key+str4+str5+child.email+str6;

                posts.push(e);

                document.getElementById('post_list').innerHTML = posts.join('');
            });
        })
        .catch(function(e) {
            console.log(e.message);
        });
}

window.onload = function() {
    loadPost();

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
			changeUserName(user);
        }
        else {
            console.log('No user logged in.');
        }
    });
};
