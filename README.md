# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : 106062202 Forum
* Key functions (add/delete)
    1. Post List updates instantaneously.
    2. Users are able to post.
    3. Users are able to comment under posts.
    
* Other functions (add/delete)
    1. Read More Button

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|N|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：https://midterm-d6970.firebaseapp.com

# Components Description : 
1. Post List updates instantaneously : Post list is added by using '''on('child_added')''' function.
2. Users are able to post : After logged in, users are directed to a page that can post with a title.
3. Users are able to comment under posts : In the list page, after clicking on READ MORE button to expand, users can click on COMMENT to comment under desired post.
...

# Other Functions Description(1~10%) : 
1. READ MORE Button : Post and comments are added dynamically by Javascript.
...

## Security Report (Optional)
